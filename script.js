let trainer = {
	name: 'Ash Ketchum',
	age: 10,
	friends: {
		hoenn: ['May','Max'],
		kanto: ['Brock','Misty']
	},
	pokemon: ["Pikachu","Charizard","Squirtle","Bulbasaur"],
	talk: function(){
		console.log('Pikachu! I choose you!');
	}
}
console.log(trainer);
console.log("Result of dot notation:");
console.log(trainer.name);
console.log("Result of square bracket notation:");
console.log(trainer['pokemon']);
console.log("Result of talk method");
trainer.talk();

function Pokemon(name,level){
	//Properties
	this.name = name;
	this.level = level;
	this.health = 2 * level;
	this.attack = level;


	// Methods
	this.tackle = function(target){
		console.log(this.name+' tackled '+target.name);
		target.health = target.health - this.attack;
		console.log(target.name+"'s health is now reduced to "+target.health);
		if (target.health <= 0) {
			console.log(target.name + " fainted");
		}
		console.log(target);
	}

}

let pikachu = new Pokemon("Pikachu",12)
let geodude = new Pokemon("Geodude",8)
let mewtwo = new Pokemon("Mewtwo",100)

console.log(geodude);
console.log(mewtwo);
console.log(pikachu);
geodude.tackle(pikachu);
mewtwo.tackle(geodude);